#ifndef TYPES_H
#define TYPES_H

typedef unsigned int Uint; // 4 bytes: 0 .. 4294967295
typedef unsigned short int Ushrt; // 2 bytes: 0 .. 65535
typedef unsigned char Uchar; // 1 byte: 0 .. 255

#define BOOL Uchar
#define FALSE 0
#define TRUE  1

#endif /* TYPES_H */
